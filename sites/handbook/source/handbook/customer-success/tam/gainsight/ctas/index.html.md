---
layout: handbook-page-toc
title: "Calls to Action (CTAs)"
description: "The Gainsight guide to using Calls to Action (CTAs) to assist you managing your accounts, making sure customers' needs are addressed, and planning for what's next."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

A [CTA](https://support.gainsight.com/Gainsight_NXT/04Cockpit_and_Playbooks/01About/CTAs_Tasks_and_Playbooks_Overview) is a call to action that is tied to a customer and appears in a TAM's cockpit, as well as the account cockpit. CTAs can be created manually at any time, but some will automatically be created for you based on certain events or data.

To create a new CTA, go to your cockpit and click "+ CTA", then fill out the appropriate information. If a CTA will consist of multiple tasks over a period of time, click CTA after saving it, then click the three dots on the top right of the new right sidebar, and click "Add Task". You can add as many as needed and track your progress in the milestone section.

Watch a quick [video on Gainsight CTAs](https://www.youtube.com/watch?v=qkjmTh3Qad4&feature=youtu.be) to learn how to use them, best practices, and tricks.

### Cockpit

The Cockpit is the main place you'll go to manage CTAs, which can be opened from several methods. They can be automatically opened due to trigger points in a customer's lifecycle, a TAM can manually open them, or if a TAM logs a task in a [Timeline](/handbook/customer-success/tam/gainsight/timeline/) event that task will populate in the Cockpit.

The main Cockpit is located by using the far left navigation panel (under "Home") and clicking "Cockpit" to see all CTAs for all customers (this can be customized and refined, discussed below).

There are also individual C360 Cockpits for each customer, which can be located by navigating to the Customer 360 and clicking "Cockpit" in the left sidebar. Please note that C360 Cockpits do not include success plan objectives, while the main Cockpit screen does.

The Cockpit can be customized for your needs so that you can see what data is most helpful to you and your workflows.

1. Use the far left nav panel (under "Home") and click "Cockpit" to see all CTAs for all customers
1. At the top right of the screen (below "Cockpit"), you'll typically want to looking at the dashboard called "My CTAs" as it is a comprehensive overview of all CTAs assigned to you (however, you can choose another default view from the list or create your own view with even further customized options)
1. Click the "Due Date" dropdown button at the top right next to the search field to change the groupings of the CTAs as desired (you can group by Type, Company, Renewal Date, etc.)
1. Click the "+" button at the right of the column names, which will take you to a list of all possible fields you can add to the column list
1. Search and add any additional fields you want to see (a common column to add that is not included by default is "Next Task Due Date")
1. Click "Apply" to save your changes to the view (you can do this as many times as you want to test out different column fields)

To create and save a customized CTA view for yourself beyond the defaults, use the filter button at the top right; you can later access these views in the dropdown at the top left.

Once you have your Cockpit customized to your needs, you can also sort the rows by a column field by clicking on the name of a column, e.g. "Due Date".

Please note that any customizations you do for a Cockpit view will be consistent between the main Cockpit and individual C360 Cockpits.


### Types of CTAs

There are 5 "types" of CTAs, which will have different corresponding playbooks, so if you're looking for a particular playbook be sure to select the appropriate type. See below for each type and the corresponding [playbooks](/handbook/customer-success/playbooks/) for that type.

* Risk
  * [Account Triage](/handbook/customer-success/tam/health-score-triage/#triage-cta)
  * Low License Utilization
  * Product Risk
* Expansion
* Lifecycle
  * Commercial Onboarding Email Series
  * Customer Offboarding
  * [Executive Business Review (EBR)](/handbook/customer-success/tam/ebr/#how-to-prepare-an-ebr)
  * [New Customer Onboarding](/handbook/customer-success/tam/onboarding/)
  * Usage Data Enablement
* Activity
  * [Handoff](/handbook/customer-success/tam/account-handoff/#account-handoff-cta)
* Renewal
  * [Upcoming Renewal](/handbook/customer-success/tam/renewals/)

If the CTA you're opening does not fall into one of the playbook categories, choose the type that makes the most sense. If it's a common CTA, suggest creating a playbook for the it by [opening an issue](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1731118?&label_name[]=gainsight).

#### CTA Status

CTA statuses are defined below.

Available on All CTAs:
* New - Brand new CTA, has yet to be started
* Work in progress - CTA currently being worked on by a TAM
* Closed Success - This CTA was completed with a positive (or intended) outcome
* Closed Lost: Not a Priority - Customer doesn't see this as a top objective to pursue
* Closed: Already Solved
* Closed: Not Relevant
* Closed: Lack of Engagement

Available on Lifecycle CTAs only:
* Closed: Not needed

Available on Objective CTAs only (Success Plans):
* Closed Lost: Competitive Loss - This is explicitly when a customer is exploring solutions for additional use cases and selects another net-new vendor. This situation does not include losing out to the incumbent, which speaks more to didn't see the value.
* Closed Lost: Product Gaps - In reviewing the use case and features/capabilities, there were product gaps, or the customer believes the product does not meet their use case needs.
* Closed Lost: Responsible Team Unengaged - Another team other than the one we are engaged with owns the capability or permission needed for adoption. We have not been successful in engaging this other team.
* Closed Lost: Didn't See Value - Customer did not see the value to justify the investment, assuming the ability to drive adoption was within their scope of control.
* Closed Lost: Deferred To Later Date - The customer has shown material interest (i.e., committed, high-level of interest) in expanding but prefers to pursue at a different date. TAM in these instances creates a reminder CTA to bring the conversation back up.
* Closed Lost: Lack of Budget - Customer would need to up-tier to use the new stage in full and does not have the budget available.
