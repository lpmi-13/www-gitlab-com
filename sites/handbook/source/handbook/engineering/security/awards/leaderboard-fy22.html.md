---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 2940 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 2 | 1440 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1240 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 4 | 1230 |
| [@manojmj](https://gitlab.com/manojmj) | 5 | 1060 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 1030 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 7 | 830 |
| [@dblessing](https://gitlab.com/dblessing) | 8 | 820 |
| [@engwan](https://gitlab.com/engwan) | 9 | 780 |
| [@theoretick](https://gitlab.com/theoretick) | 10 | 700 |
| [@xanf](https://gitlab.com/xanf) | 11 | 620 |
| [@.luke](https://gitlab.com/.luke) | 12 | 620 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 13 | 600 |
| [@vitallium](https://gitlab.com/vitallium) | 14 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 15 | 600 |
| [@leipert](https://gitlab.com/leipert) | 16 | 580 |
| [@mksionek](https://gitlab.com/mksionek) | 17 | 580 |
| [@alexpooley](https://gitlab.com/alexpooley) | 18 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 19 | 500 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 20 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 21 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 22 | 500 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 23 | 500 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 480 |
| [@10io](https://gitlab.com/10io) | 25 | 470 |
| [@balasankarc](https://gitlab.com/balasankarc) | 26 | 440 |
| [@whaber](https://gitlab.com/whaber) | 27 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 28 | 400 |
| [@wortschi](https://gitlab.com/wortschi) | 29 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 30 | 400 |
| [@brodock](https://gitlab.com/brodock) | 31 | 400 |
| [@markrian](https://gitlab.com/markrian) | 32 | 360 |
| [@mrincon](https://gitlab.com/mrincon) | 33 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 320 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 35 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 36 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 37 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 38 | 300 |
| [@matteeyah](https://gitlab.com/matteeyah) | 39 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 40 | 300 |
| [@serenafang](https://gitlab.com/serenafang) | 41 | 280 |
| [@mwoolf](https://gitlab.com/mwoolf) | 42 | 250 |
| [@kerrizor](https://gitlab.com/kerrizor) | 43 | 220 |
| [@pshutsin](https://gitlab.com/pshutsin) | 44 | 220 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 45 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 46 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 47 | 200 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 48 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 49 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 50 | 200 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 51 | 200 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 52 | 180 |
| [@seanarnold](https://gitlab.com/seanarnold) | 53 | 180 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 54 | 170 |
| [@toupeira](https://gitlab.com/toupeira) | 55 | 170 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 56 | 150 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 57 | 140 |
| [@splattael](https://gitlab.com/splattael) | 58 | 140 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 59 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 60 | 140 |
| [@twk3](https://gitlab.com/twk3) | 61 | 130 |
| [@ekigbo](https://gitlab.com/ekigbo) | 62 | 130 |
| [@nfriend](https://gitlab.com/nfriend) | 63 | 120 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 64 | 110 |
| [@cngo](https://gitlab.com/cngo) | 65 | 110 |
| [@tomquirk](https://gitlab.com/tomquirk) | 66 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 67 | 100 |
| [@cablett](https://gitlab.com/cablett) | 68 | 100 |
| [@ahegyi](https://gitlab.com/ahegyi) | 69 | 90 |
| [@minac](https://gitlab.com/minac) | 70 | 90 |
| [@vsizov](https://gitlab.com/vsizov) | 71 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 72 | 80 |
| [@ck3g](https://gitlab.com/ck3g) | 73 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 74 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 75 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 76 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 77 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 78 | 80 |
| [@acroitor](https://gitlab.com/acroitor) | 79 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 80 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 81 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 82 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 83 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 84 | 60 |
| [@lulalala](https://gitlab.com/lulalala) | 85 | 60 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 86 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 87 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 88 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 89 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 90 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 91 | 60 |
| [@garyh](https://gitlab.com/garyh) | 92 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 93 | 50 |
| [@pslaughter](https://gitlab.com/pslaughter) | 94 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 95 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 96 | 40 |
| [@afontaine](https://gitlab.com/afontaine) | 97 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 98 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 99 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 100 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 101 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 102 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 103 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@nolith](https://gitlab.com/nolith) | 2 | 300 |
| [@greg](https://gitlab.com/greg) | 3 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 4 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 5 | 200 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 6 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 7 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 8 | 80 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 9 | 80 |
| [@aqualls](https://gitlab.com/aqualls) | 10 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 11 | 30 |
| [@rymai](https://gitlab.com/rymai) | 12 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 1000 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@feistel](https://gitlab.com/feistel) | 2 | 1400 |
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 3 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 4 | 500 |
| [@behrmann](https://gitlab.com/behrmann) | 5 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 6 | 300 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 7 | 300 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 8 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 9 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 10 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 11 | 300 |
| [@tnir](https://gitlab.com/tnir) | 12 | 200 |

## FY22-Q4

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 880 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 2 | 500 |
| [@dblessing](https://gitlab.com/dblessing) | 3 | 500 |
| [@matteeyah](https://gitlab.com/matteeyah) | 4 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 5 | 300 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 6 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 7 | 120 |
| [@ekigbo](https://gitlab.com/ekigbo) | 8 | 100 |
| [@manojmj](https://gitlab.com/manojmj) | 9 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 10 | 90 |
| [@pshutsin](https://gitlab.com/pshutsin) | 11 | 80 |
| [@splattael](https://gitlab.com/splattael) | 12 | 60 |
| [@garyh](https://gitlab.com/garyh) | 13 | 60 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 14 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 15 | 50 |
| [@10io](https://gitlab.com/10io) | 16 | 30 |
| [@ahegyi](https://gitlab.com/ahegyi) | 17 | 30 |
| [@minac](https://gitlab.com/minac) | 18 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 19 | 30 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 30 |
| [@serenafang](https://gitlab.com/serenafang) | 21 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 300 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 2 | 200 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 2 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 3 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 4 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 5 | 300 |

## FY22-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1660 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 2 | 1100 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1000 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 730 |
| [@xanf](https://gitlab.com/xanf) | 5 | 620 |
| [@.luke](https://gitlab.com/.luke) | 6 | 620 |
| [@vitallium](https://gitlab.com/vitallium) | 7 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 8 | 600 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 580 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 10 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 11 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 12 | 500 |
| [@wortschi](https://gitlab.com/wortschi) | 13 | 400 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 14 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 15 | 400 |
| [@brodock](https://gitlab.com/brodock) | 16 | 400 |
| [@balasankarc](https://gitlab.com/balasankarc) | 17 | 300 |
| [@dblessing](https://gitlab.com/dblessing) | 18 | 240 |
| [@toupeira](https://gitlab.com/toupeira) | 19 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 21 | 120 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 22 | 100 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 23 | 90 |
| [@mwoolf](https://gitlab.com/mwoolf) | 24 | 90 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 25 | 90 |
| [@acroitor](https://gitlab.com/acroitor) | 26 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 27 | 80 |
| [@stanhu](https://gitlab.com/stanhu) | 28 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 29 | 80 |
| [@cngo](https://gitlab.com/cngo) | 30 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 31 | 80 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 32 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 33 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 35 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 36 | 60 |
| [@10io](https://gitlab.com/10io) | 37 | 60 |
| [@minac](https://gitlab.com/minac) | 38 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 39 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 40 | 60 |
| [@tomquirk](https://gitlab.com/tomquirk) | 41 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 42 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 43 | 60 |
| [@afontaine](https://gitlab.com/afontaine) | 44 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 45 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 46 | 40 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 47 | 30 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 48 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 49 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 50 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 51 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 52 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@smcgivern](https://gitlab.com/smcgivern) | 2 | 40 |
| [@rymai](https://gitlab.com/rymai) | 3 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 1400 |
| [@behrmann](https://gitlab.com/behrmann) | 2 | 500 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 3 | 300 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


