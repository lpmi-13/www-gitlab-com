---
layout: handbook-page-toc
title: "Highspot at GitLab"
description: "GitLab uses Highspot as its Sales Asset Management System. This page shares more about how Highspot is used at Gitlab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Highspot at GitLab

Highspot is a sales enablement tool we use to gather, categorize, and share content within GitLab and externally to our customers. It brings together native content and guidance, training and coaching, and engagement intelligence – all supported by actionable analytics. GitLab uses Highspot as a sales portal to:
1. **Provide a single repository** in which GitLab's go-to-market (GTM) team members can find all of the resources they need. 
1. **Enable more efficient discovery** of new resources and content.
1. **Drive consistency in GitLab collateral** including the branding and messaging across assets. 
1. **Attribute revenue to GitLab content** to discover ROI and enable a tighter feedback loop between Sales and Marketing.  

To learn more about Highspot's features, check out this [Highspot in Action video](https://drive.google.com/file/d/1SL27EzQUKiHSUN_QHxVlIXUrRJivhFNq/view?usp=sharing).

### Why Highspot? 

Because of the pace of business and rapid growth of the GitLab team, efficiently organizing and distributing content are key pain points for the GitLab GTM organization. 

Upon surveying members of the Field team, we found that 92% of the Field organization felt that finding the resources they needed to do their jobs was moderate-difficult ([source](https://docs.google.com/presentation/d/186Lo_e4kpUeYEbXlYEEK_Kvo1A0Ixjvcofs4MJ8AIM0/edit#slide=id.gb405fa4b55_2_25)). Furthermore, we found that 85% of the organization only finds the resources they are looking for some of the time ([source](https://docs.google.com/presentation/d/186Lo_e4kpUeYEbXlYEEK_Kvo1A0Ixjvcofs4MJ8AIM0/edit#slide=id.gb405fa4b55_2_43)). Lastly, we found that Enterprise SALs spend an average of two hours per week simply searching for Sales content, which equates to 200 hours per week across the entire Enterprise team that could be used for revenue-generating activities instead ([source](https://docs.google.com/presentation/d/186Lo_e4kpUeYEbXlYEEK_Kvo1A0Ixjvcofs4MJ8AIM0/edit#slide=id.gc0a7571506_0_5)). 

GitLab ultimately chose to onboard Highspot as a sales enablement solution / sales asset management system to increase the efficiency of marketing spend, improve sales productivity and drive more visibility around content adoption and performance. 

### Key definitions

1. **Spots** - Spots are similar to a landing page. We use spots to provide quick access to specific content for a certain group or purpose. Spots are the basic building blocks of Highspot. See [GitLab Spots](/handbook/sales/field-communications/gitlab-highspot/#gitlab-spots) for a full list of Spots. 
1. **Lists & List Groups (Role: Administrator, Co-Administrator, Editor)** - Lists work as filters, and make it easier to find the content you need. Lists can then be collected under list groups for further organization.
1. **Content owners** - Content owners are responsible for the management of content in Highspot in one or more spots. They can also make high-level changes to the search structure as needed.
   1. The key difference between Admins/Co-Admins and Editors is that Admins/Co-Admins can delete a spot while Editors cannot.
1. **Role: Employee** - This is the access level that most GitLab GTM team members will have. With this access, team members can view and pitch (for external content) all content in all Spots, but they cannot add content to Locked spots. 
1. **Contribution Status: Locked** - Only Content Owners can contribute content to these spots. All can view. 
1. **Contribution Status: Open** - GitLab team members can contribute content to these spots.
1. **Internal & External Content** - All content in Highspot will be marked as either internal or external: 
   1. **Internal** - This content is for internal consumption/enablement only. Under NO circumstances should this content be shared with external audiences - customers, prospects, partners, etc. You should also not download this content onto your local device. (You can always bookmark content to your personal spot to revisit later.)
   1. **External** - This content has been reviewed and approved for distribution to external audiences in a small forum (customer/partner call, etc.). You should *always* consult GitLab Legal before distributing content to a large audience (ex. conference or GitLab event), regardless of its distinction in Highspot. 

### GitLab Spots

| Spot Title | Description |  Viewable By | Contribution Status | 
| ------ | ------ | ------ | ------ |
| **Channel & Alliances** | Channel marketing material to share with Partners and internal GitLab team members. | All internal team members | Locked | 
| **Competitive Intelligence** | All internal competitive matters; to be used for self-education about our competitors. | All internal team members | Locked | 
| **Customer Success** | CS Operations, Gainsight, Professional Services, Solutions Architects, Technical Account Management. | All internal team members | Locked |
| **Customer-Facing Content** | Case studies, industry research (publicly shareable), collateral, email templates. | All internal team members | Locked |
| **Field Organization** | Resources sent to keep the Field team informed of organization-wide changes or large projects. | All internal team members | Locked |
| **Field Training & Enablement** | Training, onboarding, recordings, manager resources. | All internal team members | Locked |
| **Marketing** | Internal marketing collateral, personas, value drivers, use cases, internal analyst reports. | All internal team members | Locked |
| **Sales Plays** | All active and past GitLab [Sales Plays](/handbook/marketing/sales-plays/). | All internal team members | Locked |
| **Sales Contributed Content** | Collateral that Field team members have created for customers and prospects – examples of "what good looks like." | All internal team members | Locked |
| **Personal Spot** | Each team member with a Highspot account will have their own personal spot that they can use to upload content of their own. Naming convention is `[Your Account First Name] Spot`. | Account holder only | Open |

### Availability

Highspot is currently available to select GitLab team members as part of a pilot program that launched on 2021-11-15, and runs through the remainder of CY21. The tool will become generally available for all members of GitLab's go-to-market (GTM) organization in early CY22. 

Access to the tool is provisioned via SSO in Okta. To access Highspot, please navigate through the Highspot tile in your Okta dashboard.

Teams included in the GTM organization include: 
1. Sales 
1. Customer Success
1. Channel
1. Alliances 
1. Field Operations 
1. Revenue Marketing 
1. Field Marketing
<!--1. Others?<-->

### FAQs

**How do I get access to Highspot?**

Highspot is currently only available to tool administrators and a small group of team members participating in the pilot program. All members of GitLab's GTM organization will have access to the tool once it is generally available in early CY22.

**If I have a recommendation for a piece of content that should be in Highspot for all to access, how do I contact the Spot owner?**

If you have a piece of content or a resource that you think the entire GTM team could benefit from, please request that it be added to Highspot using the [Highspot Content Submission Form](https://forms.gle/62Mp1q7F82ZKQsWu8). This could be anything from a Marketing resource you found that isn't already in Highspot or a presentation/document you created/found that would be helpful for team members to reference as an example of "what good looks like." If it's helpful for the team, we want to include it in Highspot! 

**How do I determine the owner(s) of a spot?**

You can find the spot owner in the top right-hand corner of a spot or on the spot overview page, if applicable. 

**Can all team members upload content into Highspot?** 

There are only two spots in which all team members can upload content in Highspot: the Sales Contributed Spot and each team member's Personal Spot.  

**What's the best way to save a piece of content that I want to come back to later?**

You can do this by selecting `Bookmark` on the piece of content and bookmarking it into your Personal Spot. 

**How do I request to become a content owner?** 

Please request access from the manager of Field Communications & Content.

**Can partners or other external audiences access Highspot?**

Not at this time. GitLab's Highspot instance is only accessible by internal team members via SSO. External audiences can only see external content that is pitched to them via Highspot. 

We are exploring the option of a Highspot instance for GitLab partners and will share more details in FY23.

## Guidance for Highspot Content Owners

### Adding/Managing Content in Highspot
Please see the short support videos below: 

1. [Highspot Overview](https://help.highspot.com/hc/en-us/articles/360023649654-Publisher-overview-videos-#h_01EVQ9CVNRV3XD5AQB4YCT7BMZ)
1. [Create and manage spots](https://help.highspot.com/hc/en-us/articles/360023649654-Publisher-overview-videos-#h_01EVQ9MFSEZBPR8EMXV3X8K3PQ)
   - Written article: [Create a spot](https://help.highspot.com/hc/en-us/articles/360044198974-Create-a-Spot)
   - Written article: [Add links to a spot](https://help.highspot.com/hc/en-us/articles/214983663-Add-Links-to-a-Spot)
1. [Create and organize lists](https://help.highspot.com/hc/en-us/articles/360023649654-Publisher-overview-videos-#h_01EVQ9N2KA9Z1YZW6RNEN7Q9GD)
   - Written article: [Create lists and list groups](https://help.highspot.com/hc/en-us/articles/213579766-Create-lists-and-list-groups)
1. [Organize content in Highspot](https://help.highspot.com/hc/en-us/articles/360023649654-Publisher-overview-videos-#h_01EVQ9NFW02E4G86R5YKTBHCBT)
1. [Bookmark content](https://help.highspot.com/hc/en-us/articles/360023649654-Publisher-overview-videos-#h_01EVQ9P1DHESF6JSP8QDTBXBKK)

[See more articles about spots and content.](https://help.highspot.com/hc/en-us/categories/4409233846939-Spots-Content)

### GitLab Highspot Trainings 
See recordings for internal, GitLab-specific Highspot trainings below: 

1. [GitLab Highspot Content Owner Training, 2021-11-02](https://youtu.be/X07uT2G90HQ)

### GitLab Highspot Best Practices

#### Including content in more than one spot 
Some content might fit in more than one spot. When this happens, you should bookmark content into an additional spot(s). **Avoid uploading the same piece of content multiple times, as this will make version control difficult.** 

It is likely that this scenario will happen most often with the Customer-Facing Content spot, as content that fits under Marketing could also fit under Customer-Facing Content. Always default to adding the content within the primary spot first and then bookmark into Customer-Facing Content. In this scenario, you'd add the link/file to `Marketing` and then bookmark to `Customer-Facing Content`.

![Screenshot_AddBookmark](/handbook/sales/field-communications/gitlab-highspot/images/Example_AddBookmark.png)

#### Content expiration 101
Highspot includes the ability to "schedule" content. This allows you to set an expiration date for content. **We should set an expiration date on almost ALL pieces of content**, as it provides an opportunity to review that content on a regular basis and ensure that it is still relevant, the lists are still accurate, etc. If we don't set content to expire, we run the risk of letting outdated/inaccurate content exist on Highspot, compromising the efficacy of the resources. 

The only exception to this would be links or files that you know will not change. (Which is unlikely, even URLs for handbook pages can change.)

The recommendation is to set a piece of content to expire one year from the upload date. 

When a piece of content does expire, it will not be deleted. Instead, it will be moved into an Archived folder. All spot owners will receive a notification and can easily reset the content expiration date with the click of a button. 

![Screenshot_ScheduleContent](/handbook/sales/field-communications/gitlab-highspot/images/Example_ScheduleContent.png)

#### Links from Google Drive
Highspot offers the ability to link files directly from GitLab's Google Drive. **When adding a piece of content from Google Drive, you should *always* add the file directly from Drive versus add the link** – i.e. select the + button --> Add Files --> From Cloud Services (Google Drive) vs. + --> Add Link. This will allow Highspot to keep that piece of content up-to-date as changes are made to the Google Drive file. 

![Screenshot_LinkVSFile](/handbook/sales/field-communications/gitlab-highspot/images/Example_AddFiles_vs_AddLink.png)

![Screenshot_AddFromDrive](/handbook/sales/field-communications/gitlab-highspot/images/Example_AddFromDrive.png)

In addition, when adding files directly from Drive, ALWAYS select the `Automatically update content from Google Drive` option to maintain version control. 

![Screenshot_DriveAutoUpdate](/handbook/sales/field-communications/gitlab-highspot/images/Example_AutoUpdate.png)

#### Embedding Links 

When adding a link in Highspot, you will have the option to add an embed code to the content. **You should always aim to use the embed code for links when possible.** 

The format for the embed code is: `<iframe src="https://URL HERE"></iframe>`

Using the embed code will allow Highspot users to view the webpage content directly in Highspot instead of having to click out of the tool, which drives more efficiency and a more seamless user experience. Furthermore, if that link is marked as external and a Highspot user wants to pitch it out to a customer or prospect using Highspot, we will only be able to track engagement analytics on an embedded link, as a non-embedded link will cause the customer to land natively on the URL page where we cannot gather analytics. 

**For external YouTube links, you will need to use the embed URL provided by YouTube** within the embed code vs. the standard url. To do this, navigate to the YouTube video --> click `Share` underneath the video --> click `Embed` --> copy the url that is a part of the embed code shown. 

**EXCEPTIONS FOR EMBEDDING** 

*The following exceptions should be made for embedding links. Do not select `Use embed code` when adding these types of links.*
1. PathFactory - In order to allow PathFactory to gather the necessary analytics, external audiences will need to access the link that takes them directly to the asset in PathFactory.  
1. External - Some external links (ex. industry articles) will not allow embedding. You can test this by selecting `Use embed code` and then clicking on the item to check. You will see a broken link message if that link is unable to be embedded. 
1. Private YouTube Unfiltered - Links for videos set to private will not embed. 

![Screenshot_DriveAutoUpdate](/handbook/sales/field-communications/gitlab-highspot/images/Example_EmbedCode.png)

#### Internal vs. External Content 

**Only mark a resource as external if you are absolutely certain it follows the [SAFE Guidelines](/handbook/legal/safe-framework/).** Under NO circumstances should anything marked as `External` contain material non-public information including financial/performance metrics, non-approved customer logos, etc. Also ensure external content follows our [Image/Logo Guidelines](/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/) and [Trademark Guidelines](/handbook/legal/trademarks-training-materials/). 

As a good best-practice, items marked as `External` should only include decks that have been signed off by Marketing and Legal as fit for external use (ex. DevOps Platform deck) or resources that focus on the GitLab product. 

For any internal Google Drive resources, you should also select the option to `Prevent users from downloading` as GitLab team members should not keep copies of these documents on their local devices. 

If you are unsure about whether a piece of content is fit to be external, please contact GitLab Legal in the #legal channel. 

![Screenshot_DriveAutoUpdate](/handbook/sales/field-communications/gitlab-highspot/images/Example_InternalContentPermissions.png)


